---
id: vigor_protocol
title: Protocol Contract
sidebar_label: Protocol Contract
---
Details about main Vigor protocol contract actions and tables.

```
https://bloks.io/account/vigorlending
```
## Actions
### acctstake
**Account Stake:** User **{{nowrap owner}}** stakes tokens to open an account. **{{owner}}** agrees to stake tokens to open an account. RAM will deducted from **{{owner}}’s** resources to create the necessary records.   
> **fields:**
>> **name:** owner  
>> **type:** name

### assetout
**Asset Out:** Schedule an outbound token transfer, **{{usern}}** agrees to schedule the transfer of **{{assetout}}** out of the contract to **{{usern}}'s** account according to instructions on memo. **{#if memo}}** There is a memo attached to the transfer stating: **{{memo}} {{/if}}**

> **fields:**  
>> **name:** usern  
>> **type:** name  
>>
>> **name:** assetout  
>> **type:** asset
>>  
>> **name:** memo  
>> **type:** string

### bailout
**Bailout** assigns debt and collateral to lenders on loans that need to be recapitalized. bailout assigns debt and collateral to lenders on VIGOR loans that need to be recapitalized. **{{$action.account}}** agrees that bailout assigns debt and collateral to the lender: **{{usern}}** for a loan that needs to be recapitalized.

> **fields:**
>> **name:** usern  
>> **type:** name

### bailoutup
**Bailoutup** assigns debt and collateral to lenders on crypto loans that need to be recapitalized.Bailoutup assigns debt and collateral to lenders on loans that need to be recapitalized **{{$action.account}}** agrees that bailoutup assigns debt and collateral to the lender: **{{usern}}** for a crypto loan that needs to be recapitalized.

> **fields:**
>> **name:** usern  
>> **type:** name

### cleanbailout
**Clean the bailout** table of specified batch size of rows. **{{$action.account}}** agrees to clean the bailout table of specified batch size of rows, **{{batchSize}}** rows.

> **fields:**
>> **name:** batchSize  
>> **type:** uint32

### configure
**Configure:** Set a key/value pair in the configuration table. **{{$action.account}}** agrees to set the key value pair: **{{key}}, {{value}}**.

> **fields:**
>> **name:** key  
>> **type:** name
>>
>> **name:** value  
>> **type:** string

### deleteacnt
**Delete Account:** The user account for **{{nowrap owner}}** is deleted. **{{owner}}** agrees to delete their user account. Token balances on the account must be zero.

> **fields:**
>> **name:** owner  
>> **type:** name

### doassetout
**Do Asset Out:** Process a previously scheduled outbound token transfer. This action is meant to either be run as a deferred transaction initiated by the self contract, or mined. **{{$action.account}}** agrees to process a previously scheduled transfer of **{{assetout}}** out of the contract to **{{usern}}'s** account according to instructions on memo. **{{#if memo}}** There is a memo attached to the transfer stating: **{{memo}} {{/if}}.**

> **fields:**
>> **name:** usern  
>> **type:** name
>>
>> **name:** assetout  
>> **type:** asset
>>
>> **name:** memo  
>> **type:** string
>>
>> **name:** exec  
>> **type:** bool

### dodeleteacnt
**Delete Account Handler:** The user account for **{{nowrap owner}}** is deleted. **{{owner}}** agrees to delete their user account. All token balances on the account must be zero.

> **fields:**
>> **name:** owner  
>> **type:** name

### doupdate
**Update All User and Global Data:** All user and global data is updated. **{{$action.account}}** agrees to update all user and global information on the contract. This action updates all pricing, risk, performance, and runs bailouts if needed.

> **fields:**
>> **name:** i  
>> **type:** uint64

### doupdateold
**Update All User and Global Data as inline:** All user and global data is updated as inline. **{{$action.account}}** agrees to update all user and global information on the contract. This action updates all pricing, risk, performance, and runs bailouts if needed.

> **fields:**
>> **name:** i  
>> **type:** uint64

### freezelevel
**Freeze Level:** Set a key/value pair in the configuration table for the freeze level. The freeze level will restrict users from interacting with certain actions in the contract. **{{$action.account}}** agrees to set the key value pair: freezelevel, **{{value}}**.

> **fields:**
>> **name:** value  
>> **type:** string

### kick
Remove a user from the system. Removes a user from the system after unwinding any borrows and returning tokens to user. **{{$action.account}}** agrees to removes **{{usern}}** from the system after unwinding any borrows and returning tokens to user.

> **fields:**
>> **name:** usern  
>> **type:** name

### liquidate
Self liquidation for VIGOR debt - cost is a reduction of reputation. User can quickly exit a loan. User gives debt to insuers/lenders along with an equivalent value of collateral (user retaining excess collateral). **{$action.account}}** agrees that **{{usern}}** gives VIGOR debt to insuers/lenders along with an equivalent value of collateral (user retaining excess collateral).

> **fields:**
>> **name:** usern  
>> **type:** name

### liquidateup
Self liquidationup for crypto debt (not VIGOR) - cost is a reduction of reputation. User can quickly exit a loan. User gives debt to insuers/lenders along with an equivalent value of collateral (user retaining excess collateral). **{{$action.account}}** agrees that **{{usern}}** gives crypto debt (not VIGOR) to insuers/lenders along with an equivalent value of collateral (user retaining excess collateral).

> **fields:**
>> **name:** usern  
>> **type:** name

### log
Writes a message into the log table. **{{$action.account}}** agrees to write a message into the log table.

> **fields:**
>> **name:** message  
>> **type:** string

### openaccount
**Open Account:** A user account is opened for **{{nowrap owner}}**. **{{owner}}** agrees to open a user account. RAM will deducted from **{{owner}}’s** resources to create the necessary records.

> **fields:**
>> **name:** owner  
>> **type:** name

### predoupdate
**Set the step setting to desired level:** Set the step setting to desired level for batch processing through doupdate. **{{$action.account}}** sets the step setting to desired level for batch processing through doupdate.

> **fields:**
>> **name:** i  
>> **type:** uint64

### returncol
All user collateral tokens are returned to them. **{{$action.account}}** agrees that all user collateral tokens are returned to: **{{usern}}.**

> **fields:**
>> **name:** usern  
>> **type:** name

### returnins
All user insurance tokens are returned to them. **{{$action.account}}** agrees that all user insurance tokens are returned to: **{{usern}}**.

> **fields:**
>> **name:** usern  
>> **type:** name

### setacctsize
Set the limit on total account size. **{{$action.account}}** agrees to set the limit on total account size for **{{usern}}** to **{{limit}}**.

> **fields:**
>> **name:** usern  
>> **type:** name
>>
>> **name:** limit  
>> **type:** uint64

### tick
Tick calls an action in the action queue as an inline action. **{{$action.account}}** agrees that tick will call an action in the actionqueue as an inline action.

### unwhitelist
Un-whitelist a token that is currently in use as collateral, insurance, borrowing. **{{$action.account}}** agrees to whitelist token symbol: **{{sym}}** with contract **{{contract}}** and price feed name **{{feed}}**.

> **fields:**
>> **name:** sym  
>> **type:** symbol

### whitelist
Whitelist a token for use as collateral, insurance, borrowing (or update the contract and feed for an already existing token). **{{$action.account}}** agrees to whitelist token symbol: **{{sym}}** with contract **{{contract}}** and price feed name **{{feed}}**.

> **fields:**
>> **name:** sym  
>> **type:** symbol
>>
>> **name:** contract  
>> **type:** name
>>
>> **name:** feed  
>> **type:** name

## Tables

### acctsize
> whitelist for max dollar limit of total account value

### bailout
> **id (key)**
>
> **timestamp**
>	
> **usern**
>	
> **bailoutid**
>	
> **type**
>	
> **pcts**
>	
> **debt**
>	
> **collateral**
>	
> **recap1**	
>
> **recap2**	
>
> **recap3**
>	
> **recap3b**	
>
> **blockedins**	
>
> **blockeddebt**

### candidates

### config
>
>> **alphatest**
>>> rarity for the stress test, defined as an event with probability (1-alphatest)
>
>> **soltarget**
>>> solvency target, when solvency is above or below this target then price inputs are scaled to dampen borrowing and encourage lender(s). if solvency is below target then more of the risk budget is being used than is recommended by the solvency capital requirement
>
>> **lsoltarget**
>>> l_solvency target - same as solvency target except that solvency is in regards to downside market stress and l_solvency is in regards to upside market stress
>
>> **maxtesprice**
>>> upper limit on the tesprice aka borrow rate, 0.5 would be a 50% annualized maximum borrow rate
>
>> **mintesprice**
>>> lower limit on the tesprice aka borrow rate, 0.005 would be a 0.5% annualized minimum borrow rate
>
>> **calibrate**
>>> a scale factor on tesprice. tesprice is based on a one year tenor as an initial condition, solvency may come to rest above or below target, calibrate can adjust that initial condition degree of freedom
>
>> **maxtesscale**
>>> upper limit on the price adjustment factor when solvency is below target. 2.0 means that the price inputs can be scaled but no more than 2x
>
>> **mintesscale**
>>> lower limit on the price adjustment factor when solvency is above target. 0.1 means that the price inputs can be scaled but no smaller than a factor of 0.1
>
>> **reservecut**
>>> percentage of fees collected paid into reserve
>	
>> **savingscut**
>>> percentage of fees collected paid to savers
>
>> **maxlends**
>>> max percentage of insurance tokens allowable to lend out
>
>> **freezelevel**
>>> lock down specific actions in the contract. 0 unfreeze, 1 freeze assetout, 2 freeze assetin, 3 freeze assetout & assetin, 4 freeze assetout & assetin & doupdate
>
>> **assetouttime**
>>> delay time in seconds for users calling assetin
>
>> **initialvig**
>>> number of VIG required to have in collateral to take new debt
>
>> **viglifeline**
>>> if user runs out of VIG, it will borrow some for them to last viglifeline days
>
>> **vigordaccut**
>>> cut of the vig fees taken by vigor dac
>
>> **newacctlim**
>>> number of new accounts limit: can create this many new accounts: newacctlim per newacctsec
>
>> **newacctsec**
>>> number of seconds for limiting new account: can create this many new accounts: newacctlim per newacctsec
>
>> **reqstake**
>>> dollar amount of VIG tokens required to stake to open an account when account limit is reached per day
>
>> **staketime**
>>> seconds to lock the user deposited new account stake
>
>> **repanniv**
>>> anniverary for reputation score to be rotated, for example every thirty days (stated in seconds)
>
>> **maxdisc**
>>> maximum discount off the borrow rate. a 10% discount would reduce a borrow rate of 20% to 18% (.2*(1-.1))
>
>> **exectype**
>>> exectution type, 1=deferred 2=mining txn 3=inline
>
>> **minebuffer**
>>> amount of time miners have to mine before jobs expire
>
>> **gasfee (key)**
>>> gas fee to pay miners for executing one transaction
>
>> **kickseconds**
>>> 0 = turn off kick user. [Units is seconds]. The lowest ranked user is kicked every kickseconds period. Kick means: tokens returned to user, user liquidated, user account deleted
>
>> **initmaxsize**
>>> total account value cannot exceed this dollar amount
>
>> **assetintime**
>>> delay time in seconds for users calling assetin
>
>> **debtceiling**
>>> upper limit on how many VIGOR can be issued from supply
>
>> **logcount**
>>> how many log messages to store in the log table
>
>> **gatekeeper**
>>> 0 = no gatekeeper, 1 = restrict to members 2 = restrict to candidates having one or more votes, 3 = restrict to custodians
>
>> **liquidate**
>>> 0 = turn off self liquidation, 1 = turn on
>
>> **accountslim**
>>> uppler limit on the number of accounts that can exist in the user table
>
>> **mincollat**
>>> limit users from taking a new borrow such that the ratio of collateral to debt is above mincollat, for example 1.11 is 110% (note some platforms use 150% or as low as 120%)
>
>> **rexswitch**
>>> 0 = turn off REX; This also specifies when to distribute REX rewards. [Units is hundreds of larimers]. Example: 20 = distribute accumulated REX rewards when it becomes 2000 larimers (0.2000 EOS)
>
>> **dataa**
>>> for a given user, this is limit for the total dollar amount of outbound transactions per day
>
>> **datab**
>
>> **datac**
>
>> **datad**
>	
>> **proxycontr**
>	
>> **proxypay**
>	
>> **dactoken**
>	
>> **oraclehub**
>	
>> **daccustodian**
>	
>> **vigordacfund**
>
>> **finalreserve**

### croneosqueue

### croneosstats
>
>> **total_count (key)**
>
>> **exec_count**
>>
>> **cancel_count**
>>
>> **expired_count**
>

### custodians

### globalstats
>
>> **solvency**
>>> Solvency (down markets) - Represents the level of capital adequacy of the system to back the low volatility payment token borrows. It is a measure that indicates if capital existing in the system is above or below the capital requirement suggested by stress testing for downside market stress events. Borrow rates increase when solvency is below target. Borrow rates decrease when solvency is above target.
>
>> **valueofcol**
>>> dollar value of collateral
>
>> **valueofins**
>>> dollar value of insurance
>
>> **scale**
>>> TES pricing model parameters are scaled to drive risk (solvency) to a target set by custodians.
>
>> **svalueofcole**
>>> model suggested dollar value of the sum of all insufficient collateral in a stressed market SUM_i [ min((1 - svalueofcoli ) * valueofcoli - debti,0) ]
>
>> **svalueofins**
>>> model suggested dollar value of the total insurance asset portfolio in a stress event. [ (1 - stressins ) * INS ]
>
>> **stressins**
>>> model suggested percentage loss that the total insurance asset portfolio would experience in a stress event
>
>> **svalueofcoleavg**
>>> model suggested dollar value of the sum of all insufficient collateral on average in down markets, expected loss
>
>> **svalueofinsavg**
>>> model suggested dollar value of the total insurance asset portfolio on average in down market
>
>> **raroc**
>>> RAROC risk adjusted return on capital. expected return on capital employed. (Revenues - Expected Losses)/ Economic Capital
>
>> **premiums**
>
>> **scr**
>>> solvency capial requirement is the dollar amount of insurance assets required to survive a sress event
>
>> **earnrate**
>>> annualized rate of return on total portfolio of insurance crypto assets, insuring for downside and upside price jumps
>
>> **savingsrate**
>>> Savings Rate - Annualized savings rate rewarded by savers for depositing VIGOR low volatility payment tokens into VIGOR Collateral. The rewards are paid to savers automatically throughout the day denominated in VIG into their Crypto Collateral. Savings rates are floating and change continuously over time proportional to borrow rates. Savings rates increase when VIGOR market price drops below $1. Savings rates decrease when VIGOR market price rises above $1. Savings rates decrease as more savers deposit VIGOR into savings.
>
>> **lastupdate**
>
>> **fee**
>
>> **availability**
>	
>> **totaldebt**
>
>> **insurance**
>>>  Cryptos and VIGOR low volatility payment token are deposited into the insurance pool to reward VIG and to insure the system against both upside and downside market stress events. Lenders agree to accept their share of bailouts (automatically get assigned ownership of failed collateral and associated debt) according to their contribution to solvency (PCTS).
>
>> **collateral**
>
>> **l_solvency**
>>> Solvency (up markets) - Represents the level of capital adequacy of the system to back the crypto borrows. It is a measure that indicates if capital existing in the system is above or below the capital requirement suggested by stress testing for upside market stress events. Borrow rates increase when solvency is below target. Borrow rates decrease when solvency is above target.
>
>> **l_valueofcol**
>>> dollar value of l_collateral
>
>> **l_scale**
>>> TES pricing model parameters are scaled to drive risk (solvency) to a target set by custodians
>
>> **l_svalueofcole**
>>> model suggested dollar value of the sum of all insufficient collateral in a stressed market SUM_i [ min((1 - svalueofcoli ) * valueofcoli - debti,0) ]
>
>> **l_svalueofins**
>>> model suggested dollar value of the total insurance asset portfolio in a stress event. [ (1 - stressins ) * INS ]
>
>> **l_svalueofcoleavg**
>>> model suggested dollar value of the sum of all insufficient collateral on average in down markets, expected loss
>
>> **l_svalueofinsavg**
>>> model suggested dollar value of the total insurance asset portfolio on average in down market
>
>> **l_premiums**
>
>> **l_scr**
>>> solvency capital requirement is the dollar amount of insurance assets required to survive a sress event
>
>> **l_fee**
>
>> **l_totaldebt**
>
>> **step**
>>> current step in doupdate. step 0 is clean, anything else means contract is updating at that step
>
>> **ac (key)**
>>> a unique id for passing to doupdate to prevent duplicate transaction fails
>
>> **savingsscale**
>>> scale factor applied to savingscut. when VIGOR feed price is below 1.0 then savingsscale is > 1.0 thus increasing the savings rate to incentivise hodling/locking VIGOR into savings (and vice versa)
>
>> **kicktimer**
>
>> **bailoutuser**
>
>> **bailoutupuser**
>
>> **bailoutid**
>
>> **adata**
>>> total dollar amount of outbound transactions for the current day, timer is _g_atimer
>
>> **bdata**
>
>> **cdata**
>
>> **atimer**
>>> timer for limiting a user total dollar amount of outbound transactions
>	
>> **l_collateral**

### log
> **id (key)**
>
> **usern**	
>
> **function**
>	
> **message**
>	
> **timestamp**
>	
> **bailoutid**

### market
> **sym**
>	
> **marketdata**

### members
> replica of the member table from dac token

### memberterms
> replica of the termsinfo table from dac token

### stake

### stat

### tseries

### user
>> **usern (key)**
>>> User's registered account name
>
>> **debt**
>>> Loans Taken Out - amount of Vigor loaned out to user
>
>> **reputation**
>	
>> **collateral**
>>> Deposited Collateral Collateral - amount of collateral user has posted for loans (Amount of each token shown)
>	
>> **insurance**
>>> Amount of insurance user has posted (Amount of each token shown)	
>
>> **valueofcol**	
>>> Deposited Collateral Value (USD) - dollar value of user's portfolio of collateral crypto assets
>
>> **valueofins**	
>>> Deposited Insurance Value (USD) - dollar value of user's portfolio of insurance crypto assets
>
>> **tesprice**	
>>> Loan Reward (APR) - annualized rate borrowers pay in periodic premiums to insure their collateral (Max 0.25, min 0.005 – This needs to be turned into a percentage x100)
>
>> **earnrate**
>>> Insurance Rewards (APR) earnrate = Annualized rate of return on user's portfolio of insurance crypto assets, insuring for downside price jumps. 
>	
>> **pcts**
>>> Percent contribution to solvency - weighted marginal contribution to risk (solvency) rescaled by sum of that
>	
>> **volcol**
>>> Volatility of the user's collateral portfolio.	
>
>> **stresscol**
>>> Model suggested percentage loss that the user's collateral portfolio would experience in a stress event.
>
>> **istresscol**
>>> Market determined implied percentage loss that the user's collateral portfolio would experience in a stress event.
>
>> **premiums**
>>> Dollar amount of premiums borrowers would pay in one year to insure their collateral.
>
>> **svalueofinsx**
>>> stressed value of insurance down - model suggested dollar value of the total insurance asset portfolio (ex the specified user) in a down market stress event
>	
>> **lastupdate**
>	
>> **l_debt**
>>> Collateral  vector of assets and a single asset na  na  Depositing cryptos as collateral allows for borrowing VIGOR low volatility payment token. Depositing VIGOR low volatility payment token allows for borrowing cryptos. Depositing VIGOR low volatility payment token will reward the savings rate. During any borrow, the user collateral is at risk of bailout if the feed value of the collateral drops below the value of debt. Users must maintain a VIG balance above zero in Collateral because VIG fees are automatically deducted on a continuous basis, otherwise the user suffers immediate bailout. A bailout is an event where user retains excess collateral but the debt and matching amount of collateral is deducted from the borrower and taken by the lenders. There is no liquidation fee or additional charges for bailout.
>	
>> **l_collateral**
>>> Debt - a single asset and vector of assets na  na  Cryptos and VIGOR low volatility payment token borrowed. Users can borrow up to a maximum 1.1 collateral ratio (value of collateral/value of debt)
>
>> **l_valueofcol**
>	
>> **l_tesprice**
>>> Crypto borrow rate % - Annualized rate that crypto borrowers pay in periodic premiums to insure their Crypto Debt against upside market moves, as a percentage of the value of the Crypto Debt. The payments are denominated in VIG and deducted automatically throughout the day from the user Crypto Collateral. Borrow rates are floating and change continuously over time based on system riskiness as measured by Solvency (up markets).
>
>> **l_pcts**
>>> PCTS (up markets) - Percent contribution to solvency for upside market stress events. It measures the extent to which a given lender improves the system Solvency (up markets). This determines how much the lender is compensated relative to other lenders. It also determines the share of bailouts that will be assigned to the lender during upside market stress events.
>	
>> **l_volcol**
>>> volatility of the user collateral portfolio
>	
>> **l_stresscol**
>>> model suggested percentage loss that the user collateral portfolio would experience in a stress event
>
>> **l_istresscol**
>>> market determined implied percentage loss that the user collateral portfolio would experience in a stress event
>	
>> **l_premiums**
>>> Crypto borrow rate $ - Over a one year period, the total dollar amount that crypto borrowers could expect to pay in periodic premiums to insure their Crypto Debt against upside market moves. The payments are denominated in VIG and deducted automatically throughout the day from the user Crypto Collateral. Borrow rates are floating and change continuously over time based on system riskiness as measured by Solvency (up markets)
>	
>> **l_svalueofinsx**
>>> stressed value of insurance up -  model suggested dollar value of the total insurance asset portfolio (ex the specified user) in an up market stress event
>	
>> **adata**
>	
>> **bdata**
>	
>> **atimer**

### whitelist
> **sym**
>	
> **contract (key)**
>
> **feed**