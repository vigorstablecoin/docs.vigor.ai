---
id: dev-tables
title: Tables
sidebar_label: Tables
---

## User table
*  **Loans Taken Out** Debt = Amount of Vigor loaned out to user
*  **Deposited Collateral** Collateral = Amount of collateral user has posted for loans (Amount of each token shown)
*  **Insurance Rewards (APR)** earnrate = 0.0; // annualized rate of return on user portfolio of insurance crypto assets, insuring for downside price jumps
*  Insurance = Amount of insurance user has posted (Amount of each token shown)
*  **Deposited Collateral Value (USD)** valueofcol = dollar value of user portfolio of collateral crypto assets
*  **Deposited Insurance Value (USD)** valueofins = dollar value of user portfolio of insurance crypto assets
*  **Loan Reward (APR)** tesprice = annualized rate borrowers pay in periodic premiums to insure their collateral (Max 0.25, min 0.005 – This needs to be turned into a percentage x100)
*  pcts = percent contribution to solvency (weighted marginal contribution to risk (solvency) rescaled by sum of that
*  volcol = volatility of the user collateral portfolio
*  stresscol = model suggested percentage loss that the user collateral portfolio would experience in a stress event.
*  Istresscol = market determined implied percentage loss that the user collateral portfolio would experience in a stress event.
*  Svalueofcol = model suggested dollar value of the user collateral portfolio in a stress event.
*  Svalueofcole = model suggested dollar amount of insufficient collateral of a user loan in a stressed market.   Min((1 – svalueofcol ) * valueofcol – debt,0) 
*  Svalueofcoleavg = model suggested dollar amount of insufficient collateral of a user loan on average in down markets, expected loss
*  premiums = dollar amount of premiums borrowers would pay in one year to insure their collateral
*  feespaid = a payment of VIG that the borrower pays into the contract for taking out a loan
*  Borrower reputation - reputation = 500; //out of 800
*  Lastupdate = ? Last block updated??
*  Num of Late Insurance Payments Latepays = Amount of time loan premiums were paid late.

## Global table
*  solvency = 1.0; // solvency, represents capital adequacy to back the low volatility payment token
*  **Total Value of Collateral (USD)** valueofcol = 0.0; // dollar value of total portfolio of borrowers crypto collateral assets
*  **Total Value of Insurance (USD)** valueofins = 0.0; // dollar value of total portfolio of insurance crypto assets
*  scale = 1.0; // TES pricing model parameters are scaled to drive risk (solvency) to a target set by custodians.
*  svalueofcole = 0.0; // model suggested dollar value of the sum of all insufficient collateral in a stressed market SUM_i [ min((1 - svalueofcoli ) * valueofcoli - debti,0) ]
*  svalueofins = 0.0; // model suggested dollar value of the total insurance asset portfolio in a stress event. [ (1 - stressins ) * INS ]
*  stressins = 0.0; // model suggested percentage loss that the total insurance asset portfolio would experience in a stress event.
*  inreserve = whenever VIG is repaid into the system, a portion of this is sent into the reverse. The reserve is there in the event of any blck swans occurring
*  totaldebt = this is the amount of VIGOR that a user has borrowed. The user will make repayments for this debt using VIG
 

## UI display layout

| display this in the UI | table_name | table_field_name                                                | display_name            | data_type                           | example_value | display_format |
|------------------------|------------|-----------------------------------------------------------------|-------------------------|-------------------------------------|---------------|----------------|
| 1                      | user       | tesprice                                                        | VIGOR borrow rate %     | double                              | 0.015         | 1.50%          |
| 1                      | user       | l_tesprice                                                      | Crypto borrow rate %    | double                              | 0.015         | 1.50%          |
| 1                      | user       | earnrate                                                        | Reward Rate               | double                              | 0.07          | 7.00%          |
| 1                      | globals    | savingsrate                                                     | Savings Rate            | double                              | 0.05          | 5.00%          |
| 1                      | user       | reputation                                                     | Reputation            | uint64_t                            | 500           | 500            |
| 1                      | globals    | Solvency                                                        | Solvency (down markets) | double                              | 1.2           | 1.2            |
| 1                      | globals    | l_solvency                                                      | Solvency (up markets)   | double                              | 3.5           | 3.5            |
| 1                      | user       | two fields are both a type of collateral: collateral and l_debt | Collateral              | vector of assets and a single asset | na            | na             |
| 1                      | user       | insurance                                                       | Insurance               | vector of assets                    | na            | na             |
| 1                      | user       | two fields are both a type of debt: debt and l_collateral       | Debt                    | a single asset and vector of assets | na            | na             |
| 1                      | user       | pcts                                                            | PCTS (down markets)     | double                              | 0.015         | 1.50%          |
| 1                      | user       | l_pcts                                                          | PCTS (up markets)       | double                              | 0.015         | 1.50%          |
| 0                      | user       | collateral                                                      | Crypto Collateral       | vector of assets                    | na            | na             |
| 0                      | user       | l_debt                                                          | VIGOR Collateral        | asset (VIGOR)                       | na            | na             |
| 0                      | user       | debt                                                            | VIGOR Debt              | asset (VIGOR)                       | na            | na             |
| 0                      | user       | l_collateral                                                    | Crypto Debt             | vector of assets                    | na            | na             |
| 0                      | user       | premiums                                                        | VIGOR borrow rate $     | double                              | 50            | $50.00         |
| 0                      | user       | l_premiums                                                      | Crypto borrow rate $    | double                              | 50            | $50.00         |

### VIGOR borrow rate %
Annualized rate that VIGOR borrowers pay in periodic premiums to insure their Crypto Collateral against downside market moves, as a percentage of VIGOR Debt. `. Borrow rates are floating and change continuously over time based on system riskiness as measured by Solvency (down markets).

### Crypto borrow rate %
Annualized rate that crypto borrowers pay in periodic premiums to insure their Crypto Debt against upside market moves, as a percentage of VIGOR Collateral. The payments are denominated in VIG and deducted automatically throughout the day from the user Crypto Collateral. Borrow rates are floating and change continuously over time based on system riskiness as measured by Solvency (up markets).

### Earn Rate
Annualized rate of return rewarded to lenders for depositing cryptos and/or VIGOR low volatility payment tokens into Insurance. Rewards are paid to lenders automatically throughout the day into their Collateral as periodic premiums denominated in VIG. Reward rates are floating and change continuously over time based on system riskiness as measured by Solvency (up markets) and Solvency (down markets).

### Savings Rate
Annualized savings rate rewarded by savers for depositing VIGOR low volatility payment tokens into VIGOR Collateral. The rewards are paid to savers automatically throughout the day denominated in VIG into their Crypto Collateral. Savings rates are floating and change continuously over time proportional to borrow rates. Savings rates increase when VIGOR market price drops below $1. Savings rates decrease when VIGOR market price rises above $1. Savings rates decrease as more savers deposit VIGOR into savings.

### Reputation
Reputation (out of 800). Users that maintain a VIG balance above zero (in Crypto Collateral) at all times during a borrow can be rewarded higher reputation and get discounted rates. If borrower VIG balance (in Crypto Collateral) drops to zero the user loan will be bailed out automatically by lenders and reputation reset to default.

### Solvency (down markets)
Represents the level of capital adequacy of the system to back the low volatility payment token borrows. It is a measure that indicates if capital existing in the system is above or below the capital requirement suggested by stress testing for downside market stress events. Borrow rates increase when solvency is below target. Borrow rates decrease when solvency is above target.

### Solvency (up markets)
Represents the level of capital adequacy of the system to back the crypto borrows. It is a measure that indicates if capital existing in the system is above or below the capital requirement suggested by stress testing for upside market stress events. Borrow rates increase when solvency is below target. Borrow rates decrease when solvency is above target.

### Collateral
Depositing cryptos as collateral allows for borrowing VIGOR low volatility payment token. Depositing VIGOR low volatility payment token allows for borrowing cryptos. Depositing VIGOR low volatility payment token will be rewarded the savings rate. During any borrow, the user collateral is at risk of bailout if the feed value of the collateral drops below the value of debt. Users must maintain a VIG balance above zero in Collateral because VIG fees are automatically deducted on a continuous basis, otherwise the user suffers immediate bailout. A bailout is an event where user retains excess collateral but the debt and matching amount of collateral is deducted from the borrower and taken by the lenders. There is no liquidation fee or additional charges for bailout.

### Insurance
Cryptos and VIGOR low volatility payment token are deposited into the insurance pool to be rewarded VIG and to insure the system against both upside and downside market stress events. Lenders agree to accept their share of bailouts (automatically get assigned ownership of failed collateral and associated debt) according to their contribution to solvency (PCTS).

### Debt
Cryptos and VIGOR low volatility payment token borrowed. Users can borrow up to a maximum 1.1 collateral ratio (value of collateral/value of debt)

### PCTS (down markets)
Percent contribution to solvency for downside market stress events. It measures the extent to which a given lender improves the system Solvency (down markets). This determines how much the lender is compensated relative to other lenders. It also determines the share of bailouts that will be assigned to the lender during downside market stress events.

### PCTS (up markets)
Percent contribution to solvency for upside market stress events. It measures the extent to which a given lender improves the system Solvency (up markets). This determines how much the lender is compensated relative to other lenders. It also determines the share of bailouts that will be assigned to the lender during upside market stress events.

### Crypto Collateral
Cryptos (not VIGOR) deposited as collateral for borrowing VIGOR low volatility payment token. Collateral is at risk of bailout if the feed value of the collateral drops below the value of debt, or if user runs out of VIG in their Crypto Collateral and therefore cannot pay the automatically collected VIG fees.

### VIGOR Collateral
VIGOR low volatility payment token deposited to be rewarded the savings rate and to be held as collateral for borrowing cryptos. During a borrow, collateral is at risk of bailout if the feed value of the collateral drops below the value of debt, or if user runs out of VIG in their Crypto Collateral and therefore cannot pay the automatically collected VIG fees.

### VIGOR Debt
VIGOR low volatility payment token borrowed. Users can borrow up to a maximum 1.1 collateral ratio (value of collateral/value of debt)

### Crypto Debt
Cryptos borrowed (not VIGOR). Users can borrow up to a maximum 1.1 collateral ratio (value of collateral/value of debt)

### VIGOR borrow rate $
Over a one year period, the total dollar amount that VIGOR borrowers could expect to pay in periodic premiums to insure their Crypto Collateral against downside market moves. The payments are denominated in VIG and deducted automatically throughout the day from the user Crypto Collateral. Borrow rates are floating and change continuously over time based on system riskiness as measured by Solvency (down markets).

### Crypto borrow rate $
Over a one year period, the total dollar amount that crypto borrowers could expect to pay in periodic premiums to insure their Crypto Debt against upside market moves. The payments are denominated in VIG and deducted automatically throughout the day from the user Crypto Collateral. Borrow rates are floating and change continuously over time based on system riskiness as measured by Solvency (up markets).