---
id: usr-borr-stable
title: Borrow VIGOR low volatility payment token
sidebar_label: Borrow VIGOR
---
## HOW TO BORROW VIGOR VIA DEMO INTERFACE
* Login to the Vigor App at:
```
https://try.vigor.ai
```
* Accounts must have collateral **[deposited](./usr-dep-coll)** before borrowing VIGOR low volatility payment token
* Find the **Debt** box
    * The box shows the amount of VIGOR tokens already borrowed (if any)
* Fill the field with the desired amount
* Click on **BORROW**
* Confirm Scatter pop-up


## HOW TO BORROW VIGOR VIA BLOKS.IO
* Go to: **bloks.io &#x2192; vigorlending &#x2192; Contract &#x2192; Actions &#x2192; assetout**
* usern: *YOURUSERNAME*
* assetout: *THE AMOUNT OF VIGOR YOU WANT TO BORROW*
* Memo: **borrow**
* Click on **Submit Transaction**
* Confirm Scatter pop-up

Learn more about the Vigor DeFi on EOS at **[https://vigor.ai/](https://vigor.ai/)**