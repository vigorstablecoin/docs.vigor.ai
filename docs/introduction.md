---
id: introduction
title: Vigor Introduction
sidebar_label: Vigor Introduction
---

## What is Vigor?

The Vigor Protocol is a decentralized and fully automated borrow, lend, and Insure application on the EOS blockchain which allows users to unlock the value of their crypto.

The Protocol employs a two-token system; the fee-utility token VIG and the low volatility VIGOR token backed by a pool of overcollateralized cryptocurrencies.  Users can deposit crypto into the lending/insurance pool for rewards or deposit VIGOR into savings for rewards.

**VIGOR** is a crypto-backed low volatility payment token utilizing the EOS blockchain. VIGOR is transparently backed by a pool of VIG tokens, and incentivizes users to deposit enough crypto collateral, so that even during stressed markets, the price defeats volatility.  VIGOR has a maximum supply of 100,000,000.  

**VIG** is a utility token used for fees, to provide access to the system, and to be used as a final reserve.  VIG premiums are recieved as a reward by lenders and savers on the VIGOR platform.  There is also some VIG locked up in the final reserve which will aid in supporting the protocol and help to absorb market shocks.  VIG has a maximum supply of 1,000,000,000

## The Vigor Platform

The VIGOR platform is a decentralized, transparent, and fully automated borrow, lend, and save DeFi application.  To learn more about how the platform works please take advantage of the walkthorugh tutorial at [app.vigor.ai](https://app.vigor.ai/user) or try out some paper trading at [try.vigor.ai](https://try.vigor.ai/user) 

### Get Familiar with the VIGOR APP and Protocol
See Vigor [FAQ's](https://medium.com/@vigordac/vigor-ai-answers-to-commonly-asked-questions-especially-for-you-de15a45f84a0)

## The Vigor DAC (Decentralized Autonomous Community)

The VIGOR Protocol and App would not be possible without the power of the VIGOR DAC.  THe Vigor DAC, or Decentalized Autonomous Community (https://www.youtube.com/watch?v=d7l9dbiPZxw), consists of willing Canidates and elected 21 Custodians.  There is no predetermined team, controller or owner of the VIGOR Protocol. VIGOR on a daily basis appoints 21 temporary Custodians, chosen among the fully free and decentralized autonomous community. Custodians are responsible for building the VIGOR protocol according to the whitepaper and securing the multisig.  To view the top 21 at any given time please see the [Vigor Custodian Board](https://vigor.ai/community#custodianBoard).

#### Registering as a candidate
New Vigor DAC Candidates are always welcome. Candidates and the 21 elected custodians all work together in a decentralized manner to build and maintain the DAC and DAPP protocol. Everything that you see here today is attributed to the hard work of hundreds of members, users, candidates and custodians.  

Prospective candidates can fill out a profile on [http://dac.vigor.ai/](http://dac.vigor.ai/) highlighting your skillset, upload a profile picture, sign the constitution, and stake 10,000 VIG. Canidates and all other users of the platform need to sign the constitution.
